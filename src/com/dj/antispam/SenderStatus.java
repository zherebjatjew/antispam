package com.dj.antispam;

import java.io.Serializable;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 23.06.13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */
public class SenderStatus implements Serializable {
    private static final long serialVersionUID = 8732079222430641981L;
    public String address;
	public String name;
	public String snippet;
	public Long personId;
	public Boolean isSpam;
	public int count;
	public Boolean read;

	public SenderStatus(String address, Long personId, int count, String snippet) {
		this.address = address;
		this.count = count;
		this.personId = personId;
		this.snippet = snippet;
	}
}
