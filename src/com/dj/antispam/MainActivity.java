package com.dj.antispam;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.content.*;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.support.v7.app.ActionBarActivity;
import com.dj.antispam.dao.SmsDao;


public class MainActivity extends ActionBarActivity {
	private SmsDao dao;
	private Preferences prefs;
	private ActionBarDrawerToggle drawerToggle;
	private View refresh = null;
	private final Map<Class<? extends Fragment>, Fragment> fragments = new HashMap<Class<? extends Fragment>, Fragment>();

	public void startOperation() {
		if (refresh != null) {
			refresh.setVisibility(View.VISIBLE);
		}
	}

	public void endOperation() {
		if (refresh != null) {
			refresh.setVisibility(View.INVISIBLE);
		}
	}

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        dao = new SmsDao(this);
        prefs = new Preferences(this);

		setContentView(R.layout.main);
		initDrawer();
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main_menu, menu);
        if (Build.VERSION.SDK_INT >= 11) {
		    MenuItem item = menu.findItem(R.id.refresh_option_item);
            if (item != null) {
                item.setActionView(R.layout.actionbar_indeterminate_progress);
                refresh = item.getActionView().findViewById(R.id.progress);
                refresh.setVisibility(View.VISIBLE);
            }
        } else {
			menu.removeItem(R.id.refresh_option_item);
		}
		setFragment(MainFragment.class);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		drawerToggle.syncState();
        importFromExistingMessages();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
//		if (item.getItemId() == R.id.home) {
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer.isDrawerOpen(Gravity.LEFT)) {
			drawer.closeDrawer(Gravity.LEFT);
		} else {
			drawer.openDrawer(Gravity.LEFT);
		}
		return true;
//		}
//		return false;
	}

	public ActionBarDrawerToggle getDrawerToggle() {
		return drawerToggle;
	}

	public Fragment setFragment(Class<? extends Fragment> fragmentClass)
	{
		startOperation();
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment fragment = getFragment(fragmentClass);
		transaction.replace(R.id.content_frame, fragment).commitAllowingStateLoss();
		endOperation();
		return fragment;
	}

	private Fragment getFragment(Class<? extends Fragment> fragmentClass)
	{

		Fragment fragment = fragments.get(fragmentClass);
		if (fragment == null) {
			try {
				Constructor constructor = fragmentClass.getConstructor();
				fragment = (Fragment) constructor.newInstance();
				if (fragment != null) {
					fragments.put(fragmentClass, fragment);
				}
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return fragment;
	}

	private void initDrawer()
	{
		DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
		if (drawer == null) return;

		ListView drawerContent = (ListView) findViewById(R.id.left_drawer);
		drawerContent.setAdapter(new ArrayAdapter<String>(
				this,
				R.layout.drawer_list_item,
				getResources().getStringArray(R.array.fragments)));
		drawerContent.setOnItemClickListener(new DrawerItemClickListener(this));
		drawerToggle = new ActionBarDrawerToggle(this, drawer,
				R.drawable.ic_drawer,
				R.string.open, R.string.close);
		drawer.setDrawerListener(drawerToggle);
	}

	private void importFromExistingMessages() {
        if (prefs.showHint()) {
            startActivity(new Intent(this, MainHintActivity.class));
            prefs.setShowHint(false);
        } else if (prefs.showImportActivity()) {
            startActivity(new Intent(this, ImportActivity.class));
			prefs.setShowImportActivity(false);
		}
	}

	@Override
    protected void finalize() throws Throwable {
        super.finalize();
        if (dao != null) {
            dao.close();
        }
    }

	@Override
	protected void onActivityResult(int code, int result, Intent intent) {
	}

	public void onDeactivateFiltration(View view) {
		((SettingsFragment) getFragment(SettingsFragment.class)).onDeactivateFiltration();
	}

	public void onActivateFiltration(View view) {
		((SettingsFragment) getFragment(SettingsFragment.class)).onActivateFiltration();
	}

	public SmsDao getDao()
	{
		return dao;
	}

	public Preferences getPrefs()
	{
		return prefs;
	}
}
