package com.dj.antispam;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class DrawerItemClickListener implements AdapterView.OnItemClickListener
{
	private final MainActivity activity;

	public DrawerItemClickListener(MainActivity activity) {
		this.activity = activity;
	}
	@Override
	public void onItemClick(final AdapterView<?> adapterView, final View view, final int i, final long l)
	{
		final ListView list = (ListView) view.getParent();
        if (list == null) return;
		list.setItemChecked(i, true);
		DrawerLayout drawer = (DrawerLayout) activity.findViewById(R.id.drawer_layout);
		if (drawer != null) {
			drawer.closeDrawer(Gravity.LEFT);
		}
		switch (i) {
			case 0:   // main view
				activity.setFragment(MainFragment.class);
				activity.getSupportActionBar().setTitle(R.string.app_name);
				break;
			case 1:   // mark spammers
				activity.startActivity(new Intent(activity, ImportActivity.class));
				break;
			case 2:   // settings
				activity.setFragment(SettingsFragment.class);
				activity.getSupportActionBar().setTitle(R.string.settings_name);
				break;
			case 3:   // restore
				activity.startActivity(new Intent(activity, RestoringActivity.class));
				break;
			case 4:
				activity.setFragment(StatFragment.class);
				activity.getSupportActionBar().setTitle(R.string.statistic_name);
		}
		activity.getDrawerToggle().syncState();
	}
}
