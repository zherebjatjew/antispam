package com.dj.antispam;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

/**
 * Created by dj on 22.12.13.
 */
public class MainHintActivity  extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_hint);
    }

    public void onClick(View view) {
        finish();
    }
}
