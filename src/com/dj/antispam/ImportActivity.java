package com.dj.antispam;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import com.dj.antispam.dao.SmsDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 23.06.13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */
public class ImportActivity extends ActionBarActivity
{
	private ImportListAdapter adapter;
	private SmsDao dao;
	private MenuItem ok = null;
	private MenuItem refresh = null;
	private int inOperation = 0;
    private AsyncTask<Void, SenderStatus, Void> filler = null;

	/**
	 * Called when the activity is first created.
	 */
	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		dao = new SmsDao(getApplicationContext());
		setContentView(R.layout.importer);
		final ListView senders = (ListView) findViewById(R.id.listView);
		adapter = new ImportListAdapter(this, dao, savedInstanceState);
		filler = new AsyncTask<Void, SenderStatus, Void>() {

			@Override
			protected void onPreExecute() {
				startOperation();
				senders.setAdapter(adapter);
			}

			@Override
			protected Void doInBackground(final Void... voids)
			{
				for (int i = 0; i < adapter.getOriginalSize(); i++) {
					SenderStatus item = adapter.cacheItem();
					if (item != null) {
						publishProgress(item);
					}
				}
				return null;
			}

			@Override
			protected void onProgressUpdate(SenderStatus... senders) {
				for (SenderStatus item : senders) {
					adapter.addItem(item);
				}
				adapter.notifyDataSetChanged();
			}

			@Override
			protected void onPostExecute(Void result) {
				endOperation();
                filler = null;
			}
		};
        filler.execute(null, null);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.import_menu, menu);
		ok = menu.findItem(R.id.okButton);
		refresh = menu.findItem(R.id.progress);
        if (Build.VERSION.SDK_INT >= 11) {
            if (refresh != null) {
                refresh.setActionView(R.layout.actionbar_indeterminate_progress);
            }
        } else {
			refresh = null;
			menu.removeItem(R.id.progress);
		}
		if (inOperation > 0) {
			if (ok != null) ok.setVisible(false);
			if (refresh != null) refresh.setVisible(true);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menu_sel_all:
				onSelectAll();
				return true;
			case R.id.menu_sel_none:
				onSelectNone();
				return true;
			case R.id.menu_sel_auto:
				onSelectAuto();
				return true;
			case R.id.okButton:
				onOk(null);
				return true;
			case R.id.home:
				NavUtils.navigateUpFromSameTask(this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		adapter.save(outState);
	}

	@Override
	public void onStop() {
        if (filler != null) {
            filler.cancel(true);
        }
        super.onStop();
		adapter.close();
	}

	public void startOperation() {
		if (inOperation == 0) {
			if (refresh != null) refresh.setVisible(true);
			if (ok != null) ok.setVisible(false);
		}
		inOperation++;
	}

	public void endOperation() {
		inOperation--;
		if (inOperation == 0) {
			if (refresh != null) refresh.setVisible(false);
			if (ok != null) ok.setVisible(true);
		}
	}

	public void onOk(View view) {
		// Save checkboxes to spam list
		new Thread(new Runnable() {
			@Override
			public void run() {
				SmsDao dao = new SmsDao(getApplicationContext());
				try {
					List<String> denied = new ArrayList<String>();
					List<String> allowed = new ArrayList<String>();
					for (int i = 0; i < adapter.getCount(); i++) {
						SenderStatus status = adapter.getItem(i);
						if (status.isSpam != null && status.address != null) {
							if (status.isSpam) {
								denied.add(status.address);
							} else {
								allowed.add(status.address);
							}
						}
					}
					moveToSpam(dao, denied);
					Intent intent = new Intent(getResources().getString(R.string.update_action));
					sendBroadcast(intent);
					dao.markSender(denied, true);
					dao.markSender(allowed, false);
				} finally {
					dao.close();
				}
			}
		}).start();

		NavUtils.navigateUpFromSameTask(this);
	}

	private void moveToSpam(SmsDao dao, List<String> senders) {
		String where = "address IN (" + Utils.join(senders, new Utils.Processor() {
			@Override
			public void format(StringBuilder builder, Object item) {
				builder.append(DatabaseUtils.sqlEscapeString((String) item));
			}
		}) + ")";
		ContentResolver resolver = getContentResolver();
		Cursor cur = resolver.query(Uri.parse(Utils.URI_INBOX),
				new String[]{"address, body, date, _id, thread_id"}, where, null, null);
		Map<Long,Long> threads = new HashMap<Long, Long>();
		try {
			if (cur.moveToFirst()) {
				do {
					dao.putMessage(cur.getString(0),
							cur.getLong(2),
							cur.getString(1));
					dao.incrementStatistics(SmsDao.MESSAGES_FILTERED);
					threads.put(cur.getLong(4), 1L);
				} while (cur.moveToNext());
			}
			for (Long thread_id : threads.keySet()) {
				resolver.delete(Uri.parse(Utils.URI_THREADS + thread_id), null, null);
			}
			resolver.delete(Uri.parse(Utils.URI_THREADS + "-1"), null, null);
		} finally {
            try {
			    cur.close();
            } catch (RuntimeException e) {}
		}
	}

	private void onSelectAll() {
		adapter.reset(Boolean.TRUE);
	}

	private void onSelectNone() {
		adapter.reset(Boolean.FALSE);
	}

	private void onSelectAuto() {
		adapter.reset(null);
	}
}
