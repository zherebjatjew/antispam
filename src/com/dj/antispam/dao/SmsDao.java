package com.dj.antispam.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import com.dj.antispam.SmsModel;
import com.dj.antispam.Utils;

import java.util.List;

/**
 * Antispam data storage.
 *
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 31.05.13
 * Time: 22:12
 */
public class SmsDao {
	public static final String MESSAGES_TOTAL = "messages.total";
	public static final String MESSAGES_FILTERED = "messages.filtered";
	public static final String MESSAGES_RETURNED = "messages.returned";

	private SQLiteDatabase db;
	private DbHelper helper;

	public long putMessage(String from, long datetime, String body) {
		ContentValues values = new ContentValues();
		values.put('`' + DbHelper.MESSAGES_FROM + '`', from);
		values.put(DbHelper.MESSAGES_DATETIME, datetime);
		values.put('`' + DbHelper.MESSAGES_BODY + '`', body);
		return db.insert(DbHelper.TABLE_MESSAGES, null, values);
	}

	public String getDeviceId() {
		return helper.getMeta("deviceId");
	}

	public SmsDao(Context context) {
		helper = new DbHelper(context);
		db = helper.getWritableDatabase();
	}

	public Cursor getSpamCursor() {
		return db.rawQuery("SELECT * from `messages` ORDER BY `sentAt` DESC", null);
	}


	public boolean isInSpam(String sender) {
		return !db.rawQuery("SELECT `_id` FROM `messages` WHERE `from`=? LIMIT 1", new String[]{sender}).isAfterLast();
	}

	public SmsModel getMessage(int messageId) {
		Cursor cur = db.rawQuery("SELECT `_id`, `from`, `sentAt`, `body` FROM `messages` WHERE `_id`=?",
				new String[]{Integer.toString(messageId)});
		try {
            cur.moveToFirst();
            if (cur.isAfterLast()) {
				throw new IllegalArgumentException("No message with id " + messageId);
			}
			SmsModel result = new SmsModel();
			result.id = cur.getInt(0);
			result.from = cur.getString(1);
			result.sentAt = cur.getLong(2);
			result.body = cur.getString(3);
			return result;
		} finally {
			cur.close();
		}
	}

	public void deleteMessage(int messageId) {
		db.execSQL("DELETE FROM `messages` WHERE `_id`=?", new String[]{Integer.toString(messageId)});
	}

	/**
	 * Remove all messages.
	 */
	public void clean() {
		db.execSQL("DELETE FROM `messages`");
	}

	public Boolean isSenderASpammer(String sender) {
		Cursor cur = db.rawQuery("SELECT `_id`, `spam` FROM `senders` WHERE `_id`=?", new String[]{sender});
		try {
			if (cur.isAfterLast()) {
				return null;
			} else {
				cur.moveToFirst();
				return cur.getInt(1) == 1;
			}
		} finally {
			cur.close();
		}
	}

	public void markSender(String from, Boolean spam) {
		if (spam == null) {
			db.execSQL("DELETE FROM `senders` WHERE `_id`=?", new String[]{from});
		} else {
			db.execSQL("REPLACE INTO `senders` (`_id`, `spam`) VALUES (?, ?)", new Object[]{from, spam?1:0});
		}
	}

	public void markSender(List<String> senders, final Boolean spam) {
		if (spam == null) {
			db.execSQL("DELETE FROM `senders` WHERE `_id` IN (" + Utils.join(senders, new Utils.Processor() {
				@Override
				public void format(StringBuilder builder, Object item) {
					builder.append(item);
				}
			}) + ")");
		} else {
			for (String item : senders) {
				db.execSQL("REPLACE INTO `senders` (`_id`, `spam`) VALUES (" + DatabaseUtils.sqlEscapeString(item) + "," + (spam ? "1" : "0") + ')');
			}
		}
	}

	public Statistic getStatistic() {
		Statistic result = new Statistic();
		try {
			result.messages = Integer.parseInt(helper.getMeta(MESSAGES_TOTAL));
		} catch (Exception e) {
			result.messages = 0;
		}
		try {
			result.filtered = Integer.parseInt(helper.getMeta(MESSAGES_FILTERED));
		} catch (Exception e) {
			result.filtered = 0;
		}
		try {
			result.returned = Integer.parseInt(helper.getMeta(MESSAGES_RETURNED));
		} catch (Exception e) {
			result.returned = 0;
		}
		return result;
	}

	public void incrementStatistics(String key) {
		int value = 1;
		try {
			value = Integer.parseInt(helper.getMeta(key)) + 1;
		} catch (Exception e) {}
		helper.setMeta(key, Integer.toString(value));
	}

	public void close() {
		db.close();
	}
}
