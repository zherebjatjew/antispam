package com.dj.antispam.dao;

public class Statistic
{
	public int messages;	// number of total messages received
	public int filtered;	// number of messages moved to spam
	public int returned;	// number of messages, returned from spam
}
