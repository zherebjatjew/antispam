package com.dj.antispam.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 31.05.13
 * Time: 22:55
 * To change this template use File | Settings | File Templates.
 */
public class DbHelper extends SQLiteOpenHelper {
	public static final String TABLE_MESSAGES = "messages";
	public static final String MESSAGES_ID = "_id";
	public static final String MESSAGES_FROM = "from";
	public static final String MESSAGES_DATETIME = "sentAt";
	public static final String MESSAGES_BODY = "body";

	private static final String DB_NAME = "db";
	private static final int DB_VERSION = 2;
	public static final String DB_CREATE_SENDERS_TABLE = "CREATE TABLE `senders` (`_id` VARCHAR(20) PRIMARY KEY, `spam` BOOL, `addedAt` TIMESTAMP);";
	private static final String DB_CREATE_MESSAGES =
			"CREATE TABLE `messages` (`_id` INTEGER PRIMARY KEY, `from` VARCHAR(20) NOT NULL, `sentAt` DATETIME, `body` TEXT);";
	private static final String DB_CREATE_META =
			"CREATE TABLE `meta` (`_id` VARCHAR(30) PRIMARY KEY, `value` VARCHAR(150));";
	private static final String DB_ASSIGN_UID =
			"INSERT INTO `meta` (`_id`, `value`) VALUES ('deviceId', '%s'); ";
	private static final String SQL_GET_META = "SELECT `value` FROM `meta` WHERE `_id`=?";
	private static final String SQL_PUT_META = "REPLACE INTO `meta` (`_id`, `value`) VALUES (?, ?)";
	public static final String DB_DROP_META = "DROP TABLE IF EXISTS `meta`";

	public DbHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase sqLiteDatabase) {
		sqLiteDatabase.execSQL(DB_CREATE_SENDERS_TABLE);
		sqLiteDatabase.execSQL(DB_CREATE_META);
		sqLiteDatabase.execSQL(DB_CREATE_MESSAGES);
		String query = String.format(DB_ASSIGN_UID, UUID.randomUUID().toString());
		sqLiteDatabase.execSQL(query);
		sqLiteDatabase.rawQuery("SELECT * FROM `senders`", null);
	}

	@Override
	public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i1, int i2) {
		if (i2 == 2) {
			sqLiteDatabase.execSQL(DB_DROP_META);
			sqLiteDatabase.execSQL(DB_CREATE_META);
			String query = String.format(DB_ASSIGN_UID, UUID.randomUUID().toString());
			sqLiteDatabase.execSQL(query);
		}
	}

	public String getMeta(String key) {
        SQLiteDatabase database = getReadableDatabase();
        if (database == null) return null;
        Cursor cur = database.rawQuery(SQL_GET_META, new String[]{key});
		try {
			cur.moveToFirst();
			return cur.getString(0);
		} finally {
			cur.close();
		}
	}

	public void setMeta(String key, String value) {
		getReadableDatabase().execSQL(SQL_PUT_META, new Object[]{key, value});
	}
}
