package com.dj.antispam;

import com.dj.antispam.dao.SmsDao;

import android.support.v4.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import de.timroes.android.listview.EnhancedListView;

/**
 * Junk message list.
 */
public class MainFragment extends Fragment
{
	private static final String TAG = MainFragment.class.getSimpleName();

	private BroadcastReceiver updater;
	private Cursor cursor;
	private EnhancedListView list = null;

	private SmsDao getDao() {
		return ((MainActivity) getActivity()).getDao();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View result = inflater.inflate(R.layout.main_fragment, container, false);
		list = (EnhancedListView) result.findViewById(R.id.listView);
		cursor = getDao().getSpamCursor();
		getActivity().startManagingCursor(cursor);
		final SpamListAdapter adapter = new SpamListAdapter(getActivity(), cursor, getPrefs().getLastViewedTime());
		list.setAdapter(adapter);
        list.setDismissCallback(new EnhancedListView.OnDismissCallback() {
            @Override
            public EnhancedListView.Undoable onDismissToRight(EnhancedListView listView, int position) {
                final int id = getMessageId(position);
                adapter.removeMessage(id);
				list.postInvalidate();
                return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                        adapter.restoreMessage(id);
                        try {
                            updateList();
                        } catch (IllegalStateException e) {
                            // Activity is already closed
                        }
                    }
                    @Override
                    public void discard() {
                        deleteMessage(id);
						getDao().markSender(id, true);
                    }
                };
            }

            @Override
            public EnhancedListView.Undoable onDismissToLeft(EnhancedListView listView, int position) {
                final int id = getMessageId(position);
                adapter.removeMessage(id);
				updateList();
                return new EnhancedListView.Undoable() {
                    @Override
                    public void undo() {
                        adapter.restoreMessage(id);
                        try {
                            updateList();
                        } catch (IllegalStateException e) {
                            // Activity is already closed
                        }
                    }
                    @Override
                    public void discard() {
                        restoreMessage(id);
                    }
					@Override
					public String getTitle() {
						return getActivity().getString(R.string.message_restored);
					}
                };
            }

            private int getMessageId(int nItem) {
                Cursor cur = (Cursor) adapter.getItem(nItem);
                int col = cur.getColumnIndex("_id");
                return cur.getInt(col);
            }
        });
        list.enableSwipeToDismiss();
		list.load(savedInstanceState);

		updater = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// Wait for record updated is complete or you'll get empty record
				getActivity().stopManagingCursor(cursor);
				cursor = getDao().getSpamCursor();
				getActivity().startManagingCursor(cursor);
				adapter.changeCursor(cursor);
				list.postInvalidate();
			}
		};

		return result;
	}

	@Override
	public void onSaveInstanceState(android.os.Bundle outState) {
		if (list != null) {
			list.save(outState);
		}
	}

	private void restoreMessage(int messageId) {
		SmsModel message = getDao().getMessage(messageId);
		ContentValues values = new ContentValues();
		values.put(Utils.MESSAGE_ADDRESS, message.from);
		values.put(Utils.MESSAGE_BODY, message.body);
		values.put(Utils.MESSAGE_READ, true);
		values.put(Utils.MESSAGE_TYPE, Utils.MESSAGE_TYPE_SMS);
		values.put(Utils.MESSAGE_DATE, message.sentAt);
		deleteMessage(messageId);
        getDao().markSender(messageId, false);
		getDao().incrementStatistics(SmsDao.MESSAGES_RETURNED);
		ContentResolver contentResolver = getActivity().getContentResolver();
		Uri row = contentResolver.insert(Uri.parse(Utils.URI_INBOX), values);
        if (row == null) return;
		Cursor created = contentResolver.query(row, null, null, null, null);
        try {
		    created.moveToFirst();
		    Long threadId = created.getLong(created.getColumnIndex("thread_id"));
		    contentResolver.query(Uri.parse(Utils.URI_INBOX + "/" + threadId), null, null, null, null);
        } finally {
            try {
                created.close();
            } catch (RuntimeException e) {}
        }
    }

    private void deleteMessage(int id) {
        getDao().deleteMessage(id);
    }

	private void deleteAllMessages() {
		getDao().clean();
	}

	private void updateList()
	{
		Intent intent = new Intent(getResources().getString(R.string.update_action));
		getActivity().sendBroadcast(intent);
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().registerReceiver(updater, new IntentFilter(getResources().getString(R.string.update_action)));
	}

	@Override
	public void onPause() {
		super.onPause();
		getActivity().unregisterReceiver(updater);
		getPrefs().updateLastViewedTime();
	}

	public Preferences getPrefs()
	{
		return ((MainActivity) getActivity()).getPrefs();
	}
}
