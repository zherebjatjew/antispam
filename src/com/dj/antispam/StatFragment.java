package com.dj.antispam;

import com.dj.antispam.dao.SmsDao;
import com.dj.antispam.dao.Statistic;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class StatFragment extends Fragment
{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View result = inflater.inflate(R.layout.stat_fragment, container, false);
		ListView list = (ListView) result.findViewById(R.id.stat_list);
		list.setAdapter(new BaseAdapter()
		{
			class Item {
				String value;
				String name;
				Item(int nameId, int num) {
					name = getActivity().getResources().getString(nameId);
					value = Integer.toString(num);
				}
			}

			Item[] items;
			{
				Statistic stat = getMainActivity().getDao().getStatistic();
				items = new Item[] {
						new Item(R.string.stat_total, stat.messages),
						new Item(R.string.stat_filtered, stat.filtered),
						new Item(R.string.stat_returned, stat.returned)
				};
			}
			@Override
			public int getCount()
			{
				return items.length;
			}

			@Override
			public Object getItem(final int i)
			{
				return items[i];
			}

			@Override
			public long getItemId(final int i)
			{
				return i;
			}

			@Override
			public View getView(final int i, final View view, final ViewGroup viewGroup)
			{
				View result = getActivity().getLayoutInflater().inflate(R.layout.stat_item, viewGroup, false);
				TextView label = (TextView) result.findViewById(R.id.param_name);
				TextView value = (TextView) result.findViewById(R.id.param_value);
				Item itm = (Item) getItem(i);
				label.setText(itm.name);
				value.setText(itm.value);
				return result;
			}
		});
		return result;
	}

	public MainActivity getMainActivity()
	{
		return (MainActivity) getActivity();
	}
}
