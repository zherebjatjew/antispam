package com.dj.antispam;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.RemoteViews;

import com.dj.antispam.dao.SmsDao;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 31.05.13
 * Time: 21:17
 * To change this template use File | Settings | File Templates.
 */
public class SmsReceiver extends BroadcastReceiver {
	private static final String TAG = SmsReceiver.class.getSimpleName();
	private static final String ACTION = "android.provider.Telephony.SMS_RECEIVED";
	private static final String KEY_MESSAGE_ID = "message.id";
	private static final String KEY_NOTIFICATION_ID = "notification.id";

	private SmsFilter filter;
	private SmsDao dao;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent == null) return;
		final String action = intent.getAction();
		if (ACTION.equals(action)) {
			dao = new SmsDao(context);
			try {
				dao.incrementStatistics(SmsDao.MESSAGES_TOTAL);
				Preferences prefs = new Preferences(context);
				if (prefs.getActivationTime() <= System.currentTimeMillis()) {
					filter = new SmsFilter(context, dao);
					SmsModel smsMessage = extractSmsMessage(intent);
					processMessage(context, smsMessage);
				} else {
					Log.i(TAG, "Message passed because spam filter is deactivated");
				}
			} finally {
				dao.close();
			}
		} else if (action != null && Utils.INTENT_RETURN_TO_INBOX.equals(action)) {
			int id = intent.getIntExtra(KEY_MESSAGE_ID, -1);
			if (id == -1) {
				Log.w(TAG, "Extra " + KEY_MESSAGE_ID + " not found");
				return;
			}
			dao = new SmsDao(context);
			restoreMessage(context, id);
			int notificationId = intent.getIntExtra(KEY_NOTIFICATION_ID, 0);
			if (notificationId != 0) {
				NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
				notificationManager.cancel(notificationId);
			}
		}
	}

	private SmsModel extractSmsMessage(Intent intent) {
		Bundle bundle  = intent.getExtras();
        if (bundle == null) return null;
		Object[] pdus = (Object[]) bundle.get("pdus");
		SmsMessage[] messages = new SmsMessage[pdus.length];
		for (int i = 0; i < pdus.length; i++)
		{
			messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
		}

		SmsMessage sms = messages[0];
		String body;
		if (messages.length == 1 || sms.isReplace()) {
			body = sms.getDisplayMessageBody();
		} else {
			StringBuilder bodyText = new StringBuilder();
			for (int i = 0; i < messages.length; i++) {
				bodyText.append(messages[i].getMessageBody());
			}
			body = bodyText.toString();
		}
		SmsModel result = new SmsModel();
		result.body = body;
		result.from = messages[0].getDisplayOriginatingAddress();
		result.id = -1;
		result.sentAt = messages[0].getTimestampMillis();
		return result;
	}

	private void processMessage(final Context context, final SmsModel smsMessage) {
        if (smsMessage == null) return;
		Log.i(TAG, "Received SMS from " + smsMessage.from);
		Boolean isSpam = filter.isUnwelcome(smsMessage.from);
		if (isSpam == null) {
			Log.i(TAG, "SMS rejected due to suspicious sender");
			long id = archiveMessage(smsMessage);
			notifyOnSuspiciousSender(context, smsMessage, id);
			dao.incrementStatistics(SmsDao.MESSAGES_FILTERED);
		} else if (isSpam) {
			Log.i(TAG, "SMS rejected due to spam");
			archiveMessage(smsMessage);
			dao.incrementStatistics(SmsDao.MESSAGES_FILTERED);
		} else {
			return;
		}
		abortBroadcast();
		updateMainActivity(context);
	}

	private void updateMainActivity(final Context context)
	{
		Intent intent = new Intent(context.getResources().getString(R.string.update_action));
		context.sendBroadcast(intent);
	}

	private void notifyOnSuspiciousSender(Context context, SmsModel smsMessage, long messageId) {
		int notificationId = (int) (System.currentTimeMillis()%Integer.MAX_VALUE);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

		Intent clickIntent = new Intent(context, SmsReceiver.class);
		clickIntent.setAction(Utils.INTENT_RETURN_TO_INBOX);
		clickIntent.putExtra(KEY_MESSAGE_ID, (int)messageId);
		clickIntent.putExtra(KEY_NOTIFICATION_ID, notificationId);

        PendingIntent piToInbox = PendingIntent.getBroadcast(context, 999, clickIntent, PendingIntent.FLAG_ONE_SHOT);

		builder.setContentTitle(String.format(context.getString(R.string.note_title), smsMessage.from))
				.setSmallIcon(R.drawable.ic_note)
				.setContentIntent(PendingIntent.getActivity(
                        context, 0, new Intent(context, MainActivity.class),
                        PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_UPDATE_CURRENT));
        if (Build.VERSION.SDK_INT < 16) {
            RemoteViews content = new RemoteViews("com.dj.antispam", R.layout.notification);
            content.setTextViewText(R.id.sender, String.format(context.getString(R.string.note_title), smsMessage.from));
            content.setTextViewText(R.id.content, smsMessage.body);
            content.setOnClickPendingIntent(R.id.button, piToInbox);
            // API Level >= 11
            builder.setContent(content)
                    .setContentText(smsMessage.body);
        } else {
            builder.setStyle(new NotificationCompat.BigTextStyle().bigText(smsMessage.body))
                    .addAction(R.drawable.ic_action_undo, context.getString(R.string.back_to_inbox), piToInbox)
                    .setContentText(Utils.trunc(smsMessage.body, 100));
        }
		Notification notification = builder.build();
		notification.defaults = 0;
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(notificationId, notification);
	}

	private long archiveMessage(SmsModel smsMessage) {
		return dao.putMessage(smsMessage.from, smsMessage.sentAt, smsMessage.body);
	}

	private void restoreMessage(Context context, int id) {
		SmsModel message = dao.getMessage(id);
		ContentValues values = new ContentValues();
		values.put(Utils.MESSAGE_ADDRESS, message.from);
		values.put(Utils.MESSAGE_BODY, message.body);
		values.put(Utils.MESSAGE_READ, true);
		values.put(Utils.MESSAGE_TYPE, Utils.MESSAGE_TYPE_SMS);
		values.put(Utils.MESSAGE_DATE, message.sentAt);
		dao.deleteMessage(id);
		updateMainActivity(context);
		ContentResolver contentResolver = context.getContentResolver();
		contentResolver.insert(Uri.parse(Utils.URI_INBOX), values);
	}
}
