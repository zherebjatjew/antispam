package com.dj.antispam;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.PhoneNumberUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.dj.antispam.dao.SmsDao;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 23.06.13
 * Time: 13:36
 * To change this template use File | Settings | File Templates.
 */
public class ImportListAdapter extends BaseAdapter {

	private static final String TAG = "ImportListAdapter";
	private static final int SNIPPET_LEN = 100;

	private static final String KEY_CHECK = "import.check";
	private static final String KEY_COUNT = "import.count";
	private static final String KEY_BOOKMARK = "import.bookmark";
	private static final String KEY_ITEM = "import.item.";

	private final List<SenderStatus> senders = new LinkedList<SenderStatus>();
	private final Activity activity;
	private Boolean initialCheck = null;
	private final Pattern itemPattern = Pattern.compile(KEY_ITEM.replace(".", "\\.") + "(\\d\\d*)");
	private final SmsDao dao;
	private Cursor conversations = null;

	public ImportListAdapter(final Activity activity, SmsDao dao, Bundle inState) {
		this.activity = activity;
		this.dao = dao;
		conversations = activity.getContentResolver().query(Uri.parse("content://sms/conversations/"), null, null, null, "date DESC");
		conversations.moveToFirst();
		load(inState);
	}

	public void save(Bundle outState) {
		if (outState == null) return;
		if (initialCheck != null) {
			outState.putBoolean(KEY_CHECK, initialCheck);
		}
		outState.putInt(KEY_COUNT, senders.size());
		outState.putInt(KEY_BOOKMARK, conversations.getPosition());
		int n = 0;
		for (SenderStatus entry : senders) {
			if (entry != null) {
				outState.putSerializable(KEY_ITEM + n, entry);
			}
			n = n + 1;
		}
	}

	private void load(Bundle inState) {
		if (inState == null) return;
		int count = inState.getInt(KEY_COUNT);
		Object check = inState.get(KEY_CHECK);
		if (check == null) {
			initialCheck = null;
		} else {
			initialCheck = (Boolean) check;
		}
		senders.clear();
		if (count > 0) {
			for (String key : inState.keySet()) {
				final Matcher matcher = itemPattern.matcher(key);
				if (matcher.matches()) {
                    String group = matcher.group(1);
                    int idx = Integer.parseInt(group);
					SenderStatus status = (SenderStatus) inState.getSerializable(key);
					if (senders.size() > idx) {
						senders.set(idx, status);
					} else {
						while (senders.size() < idx) {
							senders.add(null);
						}
						senders.add(idx, status);
					}
				}
			}
			conversations.moveToFirst();
			int bookmark = inState.getInt(KEY_BOOKMARK);
			if (bookmark > 0) {
				conversations.move(bookmark);
			}
		}
	}

	public void close() {
		try {
			conversations.close();
		} catch (RuntimeException e) {}
	}

	private List<SenderStatus> getSenders() {
		return senders;
	}

	public void reset(Boolean mode) {
		initialCheck = mode;
		for (SenderStatus sender : senders) {
			if (sender != null) {
				sender.isSpam = null;
			}
		}
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				notifyDataSetChanged();
			}
		});
	}

	@Override
	public int getCount() {
		return getSenders().size();
	}

	@Override
	public SenderStatus getItem(int i) {
		if (i >= senders.size()) {
			return null;
		}
		return senders.get(i);
	}

	public SenderStatus cacheItem() {
		if (conversations.isAfterLast()) return null;
		SenderStatus result = cacheItem(conversations);
		conversations.moveToNext();
		return result;
	}

	private SenderStatus cacheItem(Cursor convs) {
		long threadId = convs.getLong(0/*convs.getColumnIndex("thread_id")*/);
		String snippet = convs.getString(2/*convs.getColumnIndex("snippet")*/);
		int count = convs.getInt(1/*convs.getColumnIndex("msg_count")*/);
		String phone = getAddressOf(threadId);
		if (phone == null) return null;
		String name = getNameOf(phone);
		if (name != null) return null;
		SenderStatus status = new SenderStatus(phone, threadId, count, Utils.trunc(snippet, SNIPPET_LEN));
		status.name = phone;
		suggestSpamStatus(status);
		return status;
	}

	private String getAddressOf(long threadId) {
		Uri uri = Uri.parse("content://sms");
		String where = "thread_id=" + threadId;
		Cursor mycursor = activity.getContentResolver().query(uri, null, where, null, null);
        if (mycursor == null) {
            return null;
        }
		String result = null;
		try {
			if (mycursor.moveToFirst()) {
				int columnIndex = mycursor.getColumnIndex(Utils.MESSAGE_ADDRESS);
				result = mycursor.getString(columnIndex);
                if (result == null) {
                    return null;
                }
				while (mycursor.moveToNext()) {
					String next = mycursor.getString(columnIndex);
					if (!result.equals(next)) {
						return null;
					}
				}
			}
		} finally {
			try {
				mycursor.close();
			} catch (RuntimeException e) {}
		}
		return result;
	}

	private String getNameOf(String address) {
		if (PhoneNumberUtils.isGlobalPhoneNumber(address)) {
			Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(address));
			Cursor cur = activity.getContentResolver().query(uri,
					new String[]{ContactsContract.PhoneLookup.DISPLAY_NAME}, null, null, null);
			try {
				if (cur.moveToFirst()) {
					return cur.getString(0);
				}
			} finally {
				if (cur != null) {
					cur.close();
				}
			}
		}
		return null;
	}

	private void suggestSpamStatus(final SenderStatus status) {
		if (status == null) return;
		if (status.isSpam != null) return;
		status.isSpam = checkSpam(status);
	}

	@Override
	public long getItemId(int i) {
		return i;
	}

	@Override
	public View getView(int i, View view, ViewGroup viewGroup) {
		final SenderStatus status = getItem(i);
		if (status == null) return null;
		final View res = activity.getLayoutInflater().inflate(R.layout.import_item, viewGroup, false);
		final CheckBox text = (CheckBox) res.findViewById(R.id.sender);
		final TextView count = (TextView) res.findViewById(R.id.messageCount);
		final TextView snippet = (TextView) res.findViewById(R.id.snippet);

		if (status.address != null) {
			text.setText(status.name == null ? status.address : status.name);
		}
		text.setChecked(status.isSpam != null && status.isSpam);
		snippet.setText(status.snippet);
		text.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
				status.isSpam = b;
			}
		});
		count.setText(Integer.toString(status.count));

		return res;
	}

	private boolean checkSpam(SenderStatus status) {
		if (initialCheck == null) {
			Boolean seen = dao.isSenderASpammer(status.address);
			if (seen != null) {
				return seen;
			}
			if (!PhoneNumberUtils.isGlobalPhoneNumber(status.address)) {
				return true;
			}
			if (status.read != null && !status.read && status.count == 1) {
				return true;
			}
			return false;
		} else {
			return initialCheck;
		}
	}

	public int getOriginalSize()
	{
		return conversations.getCount();
	}

	public void addItem(final SenderStatus item)
	{
		senders.add(item);
	}
}
