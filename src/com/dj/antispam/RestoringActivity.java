package com.dj.antispam;

import com.dj.antispam.dao.SmsDao;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class RestoringActivity extends ActionBarActivity
{
	private static final String TAG = RestoringActivity.class.getSimpleName();
	private MenuItem refresh = null;
	private MenuItem ok = null;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.restore);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.restore_menu, menu);
		ok = menu.findItem(R.id.okButton);
		refresh = menu.findItem(R.id.progress);
        if (refresh != null) {
            if (Build.VERSION.SDK_INT >= 11) {
                refresh.setActionView(R.layout.actionbar_indeterminate_progress);
            } else {
                refresh = null;
                menu.removeItem(R.id.progress);
            }
        }
        return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.okButton:
				restoreAllMessages();
				return true;
		}
		return false;
	}

	private void startOperation() {
		if (refresh != null && ok != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ok.setVisible(false);
					refresh.setVisible(true);
				}
			});
		}
	}

	private void endOperation() {
		if (refresh != null && ok != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					ok.setVisible(true);
					refresh.setVisible(false);
				}
			});
		}
	}

	private void restoreAllMessages() {
		final Activity activity = this;
		new Thread(new Runnable() {
			@Override
			public void run() {
				startOperation();
				SmsDao dao = new SmsDao(activity);
				Cursor cursor = dao.getSpamCursor();
				try {
					cursor.moveToFirst();
					while (!cursor.isAfterLast()) {
						ContentValues values = new ContentValues();
						values.put(Utils.MESSAGE_ADDRESS, cursor.getString(1));
						values.put(Utils.MESSAGE_BODY, cursor.getString(3));
						values.put(Utils.MESSAGE_READ, true);
						values.put(Utils.MESSAGE_TYPE, Utils.MESSAGE_TYPE_SMS);
						values.put(Utils.MESSAGE_DATE, cursor.getLong(2));
						getContentResolver().insert(Uri.parse(Utils.URI_INBOX), values);
						dao.deleteMessage(cursor.getInt(0));
						cursor.moveToNext();
					}
				} catch (Exception e) {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							Toast.makeText(getApplicationContext(), getString(R.string.msg_restore_error), Toast.LENGTH_LONG).show();
						}
					});
					getContentResolver().delete(Uri.parse(Utils.URI_THREADS + "-1"), null, null);
					activity.finish();
					return;
				} finally {
					cursor.close();
					updateList();
					endOperation();
				}
				try {
					getContentResolver().delete(Uri.parse(Utils.URI_THREADS + "-1"), null, null);
				} catch (Exception e) {
					Log.w(TAG, e);
				}
				runOnUiThread(new Runnable()
				{
					@Override
					public void run()
					{
						Toast.makeText(getApplicationContext(), getString(R.string.msg_restore_ok), Toast.LENGTH_LONG).show();
					}
				});
			}
		}).start();
	}

	private void updateList()
	{
		Intent intent = new Intent(getResources().getString(R.string.update_action));
		sendBroadcast(intent);
	}

}
