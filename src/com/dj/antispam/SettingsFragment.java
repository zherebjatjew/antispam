package com.dj.antispam;

import java.text.DateFormat;
import java.util.Date;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SettingsFragment extends Fragment
{
	private static final long DEACTIVATION_PERIOD = 1*24*60*60*1000;
	private static final DateFormat dateFormat = DateFormat.getDateInstance();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState)
	{
		View result = inflater.inflate(R.layout.settings_fragment, container, false);
		long activationTime = getMainActivity().getPrefs().getActivationTime();
		updateActivationInfo(result, activationTime);
		return result;
	}

	public void onDeactivateFiltration() {
		long activationTime = System.currentTimeMillis() + DEACTIVATION_PERIOD;
		getMainActivity().getPrefs().setActivationTime(activationTime);
		updateActivationInfo(null, activationTime);
	}

	public void onActivateFiltration() {
		getMainActivity().getPrefs().setActivationTime(0);
		updateActivationInfo(null, 0);
	}

	private void updateActivationInfo(View parent, final long activationTime)
	{
		if (parent == null) {
			parent = this.getView();
		}
		View info = parent.findViewById(R.id.activation_info);
		if (activationTime <= System.currentTimeMillis()) {
			info.setVisibility(View.INVISIBLE);
			info.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, 0));
			((Button) parent.findViewById(R.id.deactivate_filtration)).setText(R.string.deactivate);
		} else {
			info.setVisibility(View.VISIBLE);
			info.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
			((TextView) parent.findViewById(R.id.activation_label)).setText(getActivationMessage(activationTime));
			((Button) parent.findViewById(R.id.deactivate_filtration)).setText(R.string.extend_deactivation);
		}
	}

	private String getActivationMessage(long activationTime) {
		String date = dateFormat.format(new Date(activationTime));
		return String.format(getActivity().getResources().getString(R.string.activation_label), date);
	}

	public MainActivity getMainActivity()
	{
		return (MainActivity) getActivity();
	}
}
