package com.dj.antispam;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import com.dj.antispam.dao.DbHelper;

import java.text.DateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: dj
 * Date: 15.09.13
 * Time: 15:17
 * To change this template use File | Settings | File Templates.
 */
public class SpamListAdapter extends CursorAdapter {
	private final List<Long> removed = new ArrayList<Long>();
	private MainActivity activity;
	private long lastViewed;

	public SpamListAdapter(Activity activity, Cursor c, long lastViewed) {
		super(activity.getApplicationContext(), c, true);
		this.activity = (MainActivity) activity;
		this.lastViewed = lastViewed;
	}

    @Override
    public int getCount() {
        return super.getCount() - removed.size();
    }

    @Override
    public Object getItem(int position) {
        return super.getItem(correctPosition(position));
    }

    @Override
    public long getItemId(int position) {
        long res = super.getItemId(correctPosition(position));
        return res;
    }

    private int correctPosition(int position) {
		for (int i = 0; i <= position; i++) {
			long id = super.getItemId(i);
			if (Collections.binarySearch(removed, id) >= 0) {
				position++;
			}
		}
        return position;
    }

    private int restorePosition(int position) {
		for (int i = position; i >= 0; i--) {
			long id = super.getItemId(i);
			if (Collections.binarySearch(removed, id) >= 0) {
				position--;
			}
		}
        return position;
    }

	public void removeMessage(long id) {
		synchronized (removed) {
			int idx = Collections.binarySearch(removed, id);
			if (idx < 0) {
				idx = -(idx+1);
            }
			removed.add(idx, id);
		}
	}

	public void restoreMessage(long id) {
		synchronized (removed) {
			int idx = Collections.binarySearch(removed, id);
			if (idx >= 0) {
				removed.remove(idx);
			}
		}
	}

	@Override
	public View getView(int i, android.view.View view, android.view.ViewGroup viewGroup) {
		return super.getView(correctPosition(i), view, viewGroup);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
		return activity.getLayoutInflater().inflate(R.layout.sms_item, null, false);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView from = (TextView) view.findViewById(R.id.from);
		int colFrom = cursor.getColumnIndex(DbHelper.MESSAGES_FROM);
		String strFrom = cursor.getString(colFrom);
		from.setText(strFrom);
		TextView body = (TextView) view.findViewById(R.id.body);
		body.setText(cursor.getString(cursor.getColumnIndex(DbHelper.MESSAGES_BODY)));
		((TextView)view.findViewById(R.id.date)).setText(formatTime(cursor.getLong(cursor.getColumnIndex(DbHelper.MESSAGES_DATETIME))));
		if (cursor.getLong(cursor.getColumnIndex(DbHelper.MESSAGES_DATETIME)) < lastViewed) {
			body.setTextColor(context.getResources().getColor(R.color.read_body));
		} else {
			body.setTextColor(context.getResources().getColor(R.color.unread_body));
		}
	}

	private String formatTime(Long time) {
		return DateFormat.getDateTimeInstance().format(new Date(time));
	}
}
