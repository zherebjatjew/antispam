package com.dj.antispam;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * User: dzherebjatjew@thumbtack.net
 * Date: 7/9/13
 */
public class Preferences {
	private static final String ANTISPAM_IMPORT = "antispamImport.2";
	private static final String LAST_VIEWED = "lastViewed";
	private static final String ACTIVATION_TIME = "activationTime";
    private static final String MAIN_HINT_SHOWN = "mainHint";
	private final SharedPreferences prefs;

	public Preferences(Context context) {
		prefs = context.getSharedPreferences(ANTISPAM_IMPORT, Context.MODE_PRIVATE);
	}

	public boolean showImportActivity() {
		try {
			return prefs.getBoolean(ANTISPAM_IMPORT, true);
		} catch (Exception e) {
			return true;
		}
	}

	public void setShowImportActivity(boolean show) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putBoolean(ANTISPAM_IMPORT, show);
		editor.commit();
	}

    public boolean showHint() {
        try {
            return prefs.getBoolean(MAIN_HINT_SHOWN, true);
        } catch (Exception e) {
            return true;
        }
    }

    public void setShowHint(boolean show) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(MAIN_HINT_SHOWN, show);
        editor.commit();
    }

	public long getLastViewedTime() {
		return prefs.getLong(LAST_VIEWED, System.currentTimeMillis());
	}

	public void updateLastViewedTime() {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putLong(LAST_VIEWED, System.currentTimeMillis());
		editor.commit();
	}

	public long getActivationTime() {
		return prefs.getLong(ACTIVATION_TIME, 0);
	}

	public void setActivationTime(long milliSecs) {
		SharedPreferences.Editor editor = prefs.edit();
		editor.putLong(ACTIVATION_TIME, milliSecs);
        editor.commit();
	}
}
